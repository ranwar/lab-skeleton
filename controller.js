const logger = require("./logger");
const db = process.argv[3];
let requestsCounter = 0;
const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
    ? "./mysql_db.js"
    : "");

/**
 * This function gets a random product
 */
const getRandomProduct = async (req, res) => {
  requestsCounter++;
  logger.info(
    "Endpoint: getRandomProduct " +
    " Requests Counter: " +
    requestsCounter
  );
  const randProd = await queryRandomProduct();
  res.send(randProd);
};

/**
 * This function gets a product using the provided product id
 */
const getProduct = async (req, res) => {
  requestsCounter++;

  const { productId } = req.params;
  logger.info(
    "Endpoint: getProduct , Params: " + productId + ", Request Counter: " + requestsCounter
  );
  const product = await queryProductById(productId);
  res.send(product);
};

/**
 * This function gets all the products in database
 */
const getProducts = async (req, res) => {
  requestsCounter++;
  const { category } = req.query;
  logger.info(
    "Endpoint: getProducts , Params: " + category + ", Request Counter: " + requestsCounter
  );
  const products = await queryAllProducts(category);
  res.send(products);
};

/**
 * This function gets all the categories in database
 */
const getCategories = async (req, res) => {
  requestsCounter++;
  logger.info(
    "Endpoint: getCategories " + ", Request Counter: " + requestsCounter
  );
  const categories = await queryAllCategories();
  res.send(categories);
};

/**
 * This function gets all the orders in database
 */
const getAllOrders = async (req, res) => {
  requestsCounter++;

  logger.info(
    "Endpoint: getAllOrders " + ", Request Counter: " + requestsCounter
  );
  const orders = await queryAllOrders();
  res.send(orders);
};

/**
 * This function gets the orders for a given user
 */
const getOrdersByUser = async (req, res) => {
  requestsCounter++;

  const { userId } = req.query;
  logger.info(
    "Endpoint: getOrdersByUser , Params: " + userId + ", Request Counter: " + requestsCounter
  );
  const orders = await queryOrdersByUser(userId);
  res.send(orders);
};

/**
 * This function gets the order for the given order id
 */
const getOrder = async (req, res) => {
  requestsCounter++;
  const { orderId } = req.params;
  logger.info(
    "Endpoint: getOrder , Params: " + orderId + ", Request Counter: " + requestsCounter
  );
  const order = await queryOrderById(orderId);
  res.send(order);
};

/**
 * This function gets the user for the given user id
 */
const getUser = async (req, res) => {
  requestsCounter++;
  const { userId } = req.params;
  logger.info(
    "Endpoint: getUser , Params: " + userId + ", Request Counter: " + requestsCounter
  );
  const user = await queryUserById(userId);
  res.send(user);
};

/**
 * This function gets gets all the users in database
 */
const getUsers = async (req, res) => {
  requestsCounter++;

  const users = await queryAllUsers();
  logger.info(
    "Endpoint: getUsers , Request Counter: " + requestsCounter
  );
  res.send(users);
};

/**
 * This function inserts an order with the given user id and total amount
 */
const postOrder = async (req, res) => {
  requestsCounter++;
  const { user_id, total_amount } = req.body;
  logger.info(
    "Endpoint: postOrder , Params: " + user_id + ", " + total_amount + ", Request Counter: " + requestsCounter
  );
  const result = await insertOrder({ user_id, total_amount });
  res.send(result);
};

/**
 * This function updates an user with the given user id and updates
 * The updates can be of name, email or password
 */
const patchUser = async (req, res) => {
  requestsCounter++;

  const updates = req.body;
  const { userId } = req.params;

  logger.info(
    "Endpoint: patchUser , Params: " + userId + ", Request Counter: " + requestsCounter
  );
  const response = await updateUser(userId, updates);
  res.send(response);
};

module.exports = {
  init,
  getProduct,
  getRandomProduct,
  getCategories,
  getAllOrders,
  getOrdersByUser,
  getOrder,
  getProducts,
  getUser,
  getUsers,
  postOrder,
  patchUser,
};
