if (process.argv.length < 4) {
  console.error("Not enough command line arguments!");
  return;
}
//Test
const mode = process.argv[2];
if (mode === "rest") require("./restServer.js");
else if (mode === "grpc") require("./grpcServer.js");
