require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  console.log("DynamoDB connected!");
};

/**
 * queries the database to get all products,
 * pick a random number and choose that item
 * from the array, and returns that
 */
const queryRandomProduct = async () => {
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  items = response["Items"];

  let randomNum = Math.floor(Math.random() * items.length);
  randomProduct = items[randomNum];

  return randomProduct;
};

/**
 * queries the database to get a product by given id
 */
const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

/**
 * queries the database to get all products
 */
const queryAllProducts = async (category = "") => {
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  return response.Items;
};


/**
 * queries the database to get all categories
 */
const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

/**
 * queries the database to get all orders
 */
const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

/**
 * queries the database to get all orders for a given user id
 */
const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

/**
 * queries the database to get all orders for a given id
 */
const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

/**
 * queries the database to get user for a given id
 */
const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};


/**
 * queries the database to get all users
 */
const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

/**
 * queries the database to insert an order
 */
const insertOrder = async (order) => {
  try {
    const command = new PutCommand({
      TableName: "Orders",
      Item: {
        id: uuid.v4(),
        userId: order.user_id,
        totalAmount: order.totalAmount,
      },
    });

    const response = await docClient.send(command);
    return response.Item;
  } catch (error) {
    // console.error("Error inserting order:", error);
  }
};

/**
 * queries the database to update an user
 */
const updateUser = async (id, updates) => {
  try {
    const updateExpression = [];
    const expressionAttributeNames = {};
    const expressionAttributeValues = {};

    // Build the update expression and attribute values for each attribute to update
    if (updates.name) {
      updateExpression.push("#name = :name");
      expressionAttributeValues[":name"] = updates.name;
      expressionAttributeNames["#name"] = "name"; // Add this line
    }
    if (updates.email) {
      updateExpression.push("#email = :email");
      expressionAttributeValues[":email"] = updates.email;
      expressionAttributeNames["#email"] = "email"; // Add this line
    }
    if (updates.password) {
      updateExpression.push("#password = :password");
      expressionAttributeValues[":password"] = updates.password;
      expressionAttributeNames["#password"] = "password"; // Add this line
    }

    // Create the UpdateCommand
    const command = new UpdateCommand({
      TableName: "Users", // Replace with the name of your DynamoDB table
      Key: { id }, // Assuming id is a string
      UpdateExpression: `SET ${updateExpression.join(", ")}`,
      ExpressionAttributeValues: expressionAttributeValues,
      ExpressionAttributeNames: expressionAttributeNames,
      ReturnValues: "UPDATED_NEW", // Return the updated item
    });

    const response = await docClient.send(command);

    // The updated item will be in response.Attributes
    const updatedUser = response.Attributes;
    // console.log("User updated successfully:", updatedUser);
    return updatedUser;
  } catch (error) {
    // console.error("Error updating user:", error);
    throw error;
  }
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
