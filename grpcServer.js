const db = process.argv[3];
const PROTO_PATH = "./app.proto";
const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
    ? "./mysql_db.js"
    : "");
const { uuid } = require("uuidv4");

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});
//Test
const app = grpc.loadPackageDefinition(packageDefinition).App;
init();

/**
 * This function gets a random product
 */
async function randomProduct(call, callback) {
  const randProd = await queryRandomProduct();
  //console.log(randProd)
  callback(null, randProd);
}

/**
 * This function gets all the products in database
 */
async function allProducts(call, callback) {
  const products = await queryAllProducts();
  callback(null, { products });
}

/**
 * This function gets a product using the provided product id 
 */
async function product(call, callback) {
  //console.log(call.request)
  const { product_id } = call.request;
  const product = await queryProductById(product_id);
  // console.log(product[0])
  callback(null, product);
}

/**
 * This function gets all the categories in database
 */
async function categories(call, callback) {
  const categories = await queryAllCategories();
  callback(null, { categories });
}

/**
 * This function gets all the orders in database
 */
async function allOrders(call, callback) {
  const orders = await queryAllOrders();
  callback(null, { orders });
}

/**
 * This function gets the orders for a given user
 */
async function ordersByUser(call, callback) {
  const { id } = call.request;
  const orders = await queryOrdersByUser(id);
  callback(null, { orders });
}


/**
 * This function gets the order for the given order id
 */
async function order(call, callback) {
  const { id } = call.request;

  const order = await queryOrderById(id);
  //console.log(order, call.request)
  callback(null, order);
}

/**
 * This function gets the user for the given user id
 */
async function user(call, callback) {
  const { id } = call.request;
  const user = await queryUserById(id);
  //console.log(user, id)
  callback(null, user);
}

/**
 * This function gets gets all the users in database
 */
async function users(call, callback) {
  const users = await queryAllUsers();
  callback(null, { users });
}

/**
 * This function inserts an order with the given user id and total amount
 */
async function postOrder(call, callback) {
  const { user_id, total_amount } = call.request;
  //console.log(user_id, total_amount)
  const result = await insertOrder({ user_id, total_amount });

  callback(null, { result });
}

/**
 * This function updates an user with the given user id and updates
 * The updates can be of name, email or password
 */
async function accountDetails(call, callback) {
  const { id } = call.request;
  const user = await updateUser(id, call.request);
  // console.log(call.request)
  //console.log("user ", user)
  callback(null, user);
}

/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */

const server = new grpc.Server();
server.addService(app.service, {
  getRandomProduct: randomProduct,
  getAllProducts: allProducts,
  getProduct: product,
  getAllCategories: categories,
  getAllOrders: allOrders,
  getAllUserOrders: ordersByUser,
  getOrder: order,
  getUser: user,
  getAllUsers: users,
  postOrder: postOrder,
  patchAccountDetails: accountDetails,
});

server.bindAsync(
  "0.0.0.0:3001",
  grpc.ServerCredentials.createInsecure(),
  () => {
    console.log("App ready and listening on port 3001");
    server.start();
  }
);
