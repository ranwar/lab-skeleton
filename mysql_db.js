require("dotenv").config();
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();
const logger = require("./logger");
const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  //  try {
  //    await connect();
  //    logger.info("Database connected!");
  //  } catch (e) {
  //    console.error(e);
  //  }
};

/**
 * queries the database to get a product by given id
 */
const queryProductById = async (productId) => {
  let result = await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`);
  return result[0][0];
};

/**
 * queries the database to get a product randomly
 */
const queryRandomProduct = async () => {
  let result = await mysql.query(
    "SELECT * FROM products ORDER BY RAND() LIMIT 1;"
  );
  return result[0][0];
};

/**
 * queries the database to get all products
 */
const queryAllProducts = async () => {
  return (await mysql.query("SELECT * FROM products;"))[0];
};

/**
 * queries the database to get all categories
 */
const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

/**
 * queries the database to get all orders
 */
const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

/**
 * queries the database to get all orders for a given user id
 */
const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

/**
 * queries the database to get all orders for a given id
 */
const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

/**
 * queries the database to get user for a given id
 */
const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

/**
 * queries the database to get all users
 */
const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

/**
 * queries the database to insert an order
 */
const insertOrder = async (order) => {
  try {
    const [result] = await mysql.query(
      "INSERT INTO orders (id, user_id, total_amount) VALUES (?, ?, ?)",
      [uuid.v4(), order.user_id, order.total_amount]
    );
    return result;
  } catch (error) {
    // console.error("Error inserting order:", error);
  }
};

/**
 * queries the database to update an user
 */
const updateUser = async (id, updates) => {
  try {
    const { name, email, password } = updates;

    // Construct the SQL query to update the user
    const sql =
      "UPDATE users SET name = ?, email = ?, password = ? WHERE id = ?";

    // Execute the update query
    const [result] = await mysql.query(sql, [name, email, password, id]);

    return result;
  } catch (error) {
    // console.error("Error updating user:", error);
  }
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
